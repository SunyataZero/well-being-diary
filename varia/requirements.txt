PyQt5>=5.14
# PyQt5-sip
# sip
# https://pypi.org/project/PyQt5/#history
# https://pypi.org/project/SIP/#history

Pillow
# https://pypi.org/project/Pillow/#history

# Envelopes
# https://pypi.org/project/Envelopes/

# yagmail
# https://pypi.org/project/yagmail/#history
